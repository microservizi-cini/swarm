# Swarm

## Configurare Swarm

### Se non avete una macchine provate: [Play with Docker](https://labs.play-with-docker.com/).

#### Avviamo il nodo **master**:

```bash
node1$ docker swarm init
To add a worker to this swarm, run the following command:

  docker swarm join --token SWMTKN-1-5ur3q84en…
```

####  Avviamo un (o più) nodo(i) **worker** (Copia l'output di `init` e incollalo):

```bash
node2$ docker swarm join --token SWMTKN-1-5ur3q84en…
```

####  Controlliamo che abbia funzionato

```bash
node1$ docker node ls
ID           HOSTNAME STATUS AVAILABILITY MANAGER STATUS ENGINE VERSION
ecqr03l3 *   node1    Ready  Active       Leader         19.03.11
jt7we9zl     node2    Ready  Active                      19.03.11
```


## Inizamo!


```bash
docker stack deploy –c docker-compose.yaml app
```

* Mostrare i *services* di uno stack

```bash
docker stack services app
```

* Mostrare i *tasks* di uno stack

```bash
docker stack ps app
```

* Mostrare i log di un servzio

```bash
docker service logs app_web
```

* Fermare uno stack

```bash
$ docker stack rm app
```

---

## Scaliamo

```bash
$ docker service scale <nome_servizio>=<numero_repliche>
```
Ad esempio

```bash
$ docker service scale app_wordpress=3
```

## Svuotare un nodo

```bash
docker node update --availability drain <nome_nodo>
```
### Riabilitarlo

```bash
docker node update --availability active <nome_nodo>
```
Possiamo sempre controllare lo stato con
```bash
docker node ls
```

```bash
node1$ docker node ls                                                             Mon Oct  4 01:04:14 2021
ID                            HOSTNAME         STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
sjanejdqfoxeqax16dm8b5ah0 *   docker-desktop   Ready     Active         Leader           20.10.8
```
